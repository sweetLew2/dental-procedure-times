import React from 'react';
import { Panel } from '@fluentui/react/lib/Panel';
import { NavBar } from '../nav-bar/nav-bar';
import './nav-bar-panel.scss';

export interface INavBarPanel
{
    isOpen: boolean,
    hashRoute: string,
    dismissPanel: () => void
}

export const NavBarPanel: React.FunctionComponent<INavBarPanel> = (props) => {
    return (
        <div className='nav-bar-panel'>
            <Panel
                headerText="Procedures"
                isBlocking={false}
                isOpen={props.isOpen}
                onDismiss={props.dismissPanel}
                closeButtonAriaLabel="Close"
                className='nav-bar-panel_left'
            >
                <NavBar
                    hashRoute={props.hashRoute}
                />
            </Panel>
        </div>
    );
};
