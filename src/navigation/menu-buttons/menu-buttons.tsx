import * as React from 'react';
import { IconButton } from '@fluentui/react/lib/Button';
import { Link } from '@fluentui/react/lib/Link';
import { IOverflowSetItemProps, OverflowSet } from '@fluentui/react/lib/OverflowSet';
import menuButtons from './menu-buttons.json';
import buttonStyles from './overflow-button-styles.json';
import menuButtonStyles from './menu-button-styles.json';

export interface IMenuButtonsProps {
    onItemClick: (itemKey: string) => void,
    onHashRouteChange: (route: string) => void
}

export const MenuButtons: React.FunctionComponent<IMenuButtonsProps> = (props) => {

    const items = menuButtons.filter(x => x.overflow === false).map(x  => ({
        key: x.key,
        name: x.name,
        onClick: () => props.onItemClick(x.key)
    }));

    const overflowItems = menuButtons.filter(x => x.overflow === true).map(x => ({
        key: x.key,
        name: x.name,
        onClick: () => props.onItemClick(x.key)
    }));

    return (
        <OverflowSet
            aria-label="Basic Menu Example"
            role="menubar"
            items={items}
            overflowItems={overflowItems}
            onRenderOverflowButton={onRenderOverflowButton}
            onRenderItem={onRenderItem}
        />
    );
}

const onRenderItem = (item: IOverflowSetItemProps): JSX.Element => {
    return (
        <Link
            role="menuitem"
            styles={menuButtonStyles}
            onClick={item.onClick}>
            {item.name}
        </Link>
    );
};

const onRenderOverflowButton = (overflowItems: any[] | undefined): JSX.Element => {
    return (
        <IconButton
            role="menuitem"
            title="More options"
            styles={buttonStyles}
            menuIconProps={{ iconName: 'More' }}
            menuProps={{ items: overflowItems! }}
        />
    );
};
