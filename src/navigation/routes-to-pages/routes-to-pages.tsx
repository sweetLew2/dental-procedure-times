import React from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';
import { BoneGraftingPage } from '../../pages/bone-grafting-page/bone-grafting-page';
import { ExtractionsPage } from '../../pages/extractions-page/extractions-page';
import { GumGraftingPage } from '../../pages/gum-grafting-page/gum-grafting-page';
import { ImplantsPage } from '../../pages/implants-page/implants-page';
import { MainPage } from '../../pages/main-page/main-page';
import { MiscellaneousPage } from '../../pages/miscellaneous-page/miscellaneous-page';
import { OsseousPage } from '../../pages/osseous-page/osseous-page';
import { SplintingPage } from '../../pages/splinting-page/splinting-page';

export interface IRoutesToPagesProps
{
    setHashRoute: (newRoute: string) => void
}

export const RoutesToPages: React.FunctionComponent<IRoutesToPagesProps> = (props) => {
    const location = useLocation()

    React.useEffect(() => {
        props.setHashRoute(location.pathname);
    }, [location])

    return (
        <div className="routes-to-pages">
            <Routes>
                <Route path='/gum-grafts' element={<GumGraftingPage />} />
                <Route path='/implants' element={<ImplantsPage />} />
                <Route path='/osseous' element={<OsseousPage />} />
                <Route path='/extractions' element={<ExtractionsPage />} />
                <Route path='/bone-grafts' element={<BoneGraftingPage />} />
                <Route path='/splints' element={<SplintingPage />} />
                <Route path='/miscellaneous' element={<MiscellaneousPage />} />
                <Route path='*' element={<MainPage />} />
                <Route index element={<MainPage />} />
            </Routes>
        </div>
    )
}
