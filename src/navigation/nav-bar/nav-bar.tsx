import * as React from 'react';
import { Nav } from '@fluentui/react/lib/Nav';
import navLinkGroups from './nav-bar.json';
// import { useLocation } from 'react-router-dom';
import { getKeyFromHash } from './get-key-from-hash';

export interface INavBarProps
{
    hashRoute: string,
}

export const NavBar: React.FunctionComponent<INavBarProps> = (props) => {
    const selectedKey = getKeyFromHash(navLinkGroups, props.hashRoute);
    return (
        <Nav
            className="ms-Fabric"
            ariaLabel="Nav example with nested links"
            groups={navLinkGroups}
            selectedKey={selectedKey}
        />
    );
};
