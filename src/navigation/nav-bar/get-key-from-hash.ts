import { INavLinkGroup } from "@fluentui/react";

export const nonMatchingKey = '_';

// Note: an `undefined` return type is causes the underlying tool to handle it's own source of truth
export const getKeyFromHash = (navLinkGroups: INavLinkGroup[] | null, locationHash: string): string | undefined =>
{
    if (!navLinkGroups || !navLinkGroups[0].links)
    {
        return nonMatchingKey;
    }

    const foundIndex = navLinkGroups[0].links.findIndex(x => x.url === `#${locationHash}`);
    if (foundIndex === -1)
    {
        return nonMatchingKey;
    }

    const key = navLinkGroups[0].links[foundIndex].key;
    if (!key)
    {
        return nonMatchingKey;
    }

    return key;
}
