import React from 'react';
import './App.scss';
import { initializeIcons } from '@fluentui/react/lib/Icons';
import { PageLayout } from './page-layout/page-layout';
import { HashRouter } from 'react-router-dom';

function App() {
    initializeIcons();

    return (
        <div className="App">
            <HashRouter>
                <PageLayout />
            </HashRouter>
        </div>
    );
}

export default App;
