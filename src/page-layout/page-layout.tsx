import React from 'react';
import { useBoolean } from '@fluentui/react-hooks';
import { NavBarPanel } from '../navigation/nav-bar-panel/nav-bar-panel';
import { MenuButtons } from '../navigation/menu-buttons/menu-buttons';
import { MenuSectionCompanyLogo } from '../navigation/menu-section-company-logo/menu-section-company-logo';
import './page-layout.scss';
import { RoutesToPages } from '../navigation/routes-to-pages/routes-to-pages';

export const PageLayout: React.FunctionComponent = () => {
    const [isOpen, { setTrue: openPanel, setFalse: dismissPanel }] = useBoolean(false);
    const [hashRoute, setHashRoute] = React.useState("/");

    const handleMenuButtonClick = (itemKey: string) =>
    {
        if (itemKey === 'procedures')
        {
            openPanel();
        }
    }

    return (
        <div className="page-layout">
            <div className="page-header">
                <MenuSectionCompanyLogo />
                <MenuButtons
                    onItemClick={handleMenuButtonClick}
                    onHashRouteChange={(newRoute) => setHashRoute(newRoute)}
                />
            </div>
            <div className="page-body">
                <NavBarPanel
                    isOpen={isOpen}
                    dismissPanel={dismissPanel}
                    hashRoute={hashRoute}
                />
                <RoutesToPages
                    setHashRoute={setHashRoute}
                />
            </div>
        </div>
    );
};
