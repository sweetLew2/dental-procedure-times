import React from 'react';

export const MainPage: React.FunctionComponent = () => {
    return (
        <div className="main-page">
            <span className="ms-Fabric">
                <h1>Main Page</h1>
            </span>
        </div>
    )
}